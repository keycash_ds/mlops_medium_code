"""
ModelHandler defines an example model handler for load and inference requests for ML Models
"""
import json
import os
import pickle
import pandas as pd
import json

class ModelHandler(object):
    """
    A sample Model handler implementation.
    """
    def __init__(self):
        self.initialized = False
        # self.mx_model = None
        # self.shapes = None


    def initialize(self, context):
        """
        Initialize model. This will be called during model loading time
        :param context: Initial context contains model server system properties.
        :return:
        """
        self.initialized = True
        properties = context.system_properties
        # Contains the url parameter passed to the load request
        model_dir = properties.get("model_dir")

        # Load our own model
        with open(os.path.join(model_dir, 'model.sav'), 'rb') as inp:
            self.model = pickle.load(inp)
            
    def preprocess(self, request):
        """
        Transform raw input into model input data.
        :param request: list of raw requests
        :return: list of preprocessed model input data
        """

        request = request[0]
        json_request = request.get('body').decode('utf8')
        # Convert json into pandas DataFrame
        string_json_request = json.loads(json_request)
        dataframe = pd.DataFrame.from_dict(string_json_request, orient="index")
        return dataframe
        
    def inference(self, model_input): 
        """
        Internal inference methods
        :param model_input: transformed model input data list
        :return: list of inference output in NDArray
        """
        predicted = self.model.predict(model_input)
        return predicted

    def postprocess(self, inference_output):
        """
        Return predict result in as list.
        :param inference_output: list of inference output
        :return: list of predict results
        """
        # Convert a DataFrame into JSON
        predicted_df = pd.DataFrame(inference_output)
        predicted_json = predicted_df.to_json()
        return [predicted_json.encode('ascii')]

    def handle(self, data, context):
        """
        Call preprocess, inference and post-process functions
        :param data: input data
        :param context: mms context
        """
        model_input = self.preprocess(data)
        model_out = self.inference(model_input)
        return self.postprocess(model_out)

_service = ModelHandler()

def handle(data, context):
    if not _service.initialized:
        _service.initialize(context)

    if data is None:
        return None

    return _service.handle(data, context)