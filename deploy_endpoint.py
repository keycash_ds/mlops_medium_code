import boto3

# =============== Connections ====================
sm_client = boto3.client(service_name="sagemaker")
runtime_sm_client = boto3.client(service_name="sagemaker-runtime")


# =============== Import models into hosting ====================
model_name = 'MultiModel'
model_url = f's3://<main-bucket>/<models_dir>/<multiple_models>/'
container_image = f'{account_id}.dkr.ecr.{region}.amazonaws.com/demo-sagemaker-multimodel:latest'
container = {"Image": container_image, "ModelDataUrl": model_url, "Mode": "MultiModel"}
create_model_response = sm_client.create_model(
    ModelName=model_name, 
    ExecutionRoleArn=<sagemaker_execution_role>, 
    Containers=[container]
)

# ================ Endpoint configuration ====================
endpoint_config_name = 'MultiModelEndpointConfig'
create_endpoint_config_response = sm_client.create_endpoint_config(
    EndpointConfigName=endpoint_config_name,
    ProductionVariants=[    
        {
            "InstanceType": "ml.t2.medium",
            "InitialInstanceCount": 1,
            "InitialVariantWeight": 1,
            "ModelName": model_name,
            "VariantName": "AllTraffic",
        }
    ],
)

# ================ Create endpoint ====================
endpoint_name = "MultiModelEndpoint"
create_endpoint_response = sm_client.create_endpoint(
    EndpointName=endpoint_name, EndpointConfigName=endpoint_config_name
)
resp = sm_client.describe_endpoint(EndpointName=endpoint_name)
status = resp["EndpointStatus"]
print("Endpoint Status: " + status)

print("Waiting for {} endpoint to be in service...".format(endpoint_name))
waiter = sm_client.get_waiter("endpoint_in_service")
waiter.wait(EndpointName=endpoint_name)